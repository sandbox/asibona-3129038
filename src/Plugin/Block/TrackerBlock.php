<?php

namespace Drupal\commerce_block_tracking_order\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'dogmautil' Block.
 *
 * @Block(
 *   id = "tracker_block",
 *   admin_label = @Translation("BTR tracking"),
 *   category = @Translation("Dogma"),
 * )
 */
class TrackerBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $routeMetch = \Drupal::routeMatch();
    if($routeMetch->getRouteName() != 'entity.commerce_order.user_view') {
      return ['#markup' => t("Errore nella configuraaione del blocco")];
    }
    $build = [];
    $user_id = $routeMetch->getRawParameter('user');
    $account = \Drupal::currentUser();
    if($account->id() != 1 && ($account->id() != $user_id)) { return ['#markup' => ""]; }
    $build['#markup'] = t("Il tracciamento del pacco al momento non è disponibile.");
    $order = \Drupal\commerce_order\Entity\Order::load(
      $routeMetch->getRawParameter('commerce_order')
    );
    if ($order->hasField('shipments') && !$order->get('shipments')->isEmpty()) {
      $shipments = $order->get('shipments')->referencedEntities()[0];
      if($shipments->hasField('tracking_code') && !$shipments->get('tracking_code')->isEmpty()) {
        $build['#attached']['drupalSettings']['dogma_brt']['code'] = $shipments->get('tracking_code')->get(0)->value;
        $build['#attached']['drupalSettings']['dogma_brt']['lenguage'] = $language;
        $build['#markup'] = "<div id=\"tracking_brt\"><div id=\"TRNum\"> </div></div>";
        $build['#attached']['library'][] = 'dogmautil/dogmautil';
      }
    }
    return $build;
  }
}
